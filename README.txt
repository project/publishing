The publishing module is primarily a glue module to make it easier to handle
article publication in a standard form.

It helps you define and create a workflow, provides a page to make it easy to
see what state the articles are in, and provides tools to help ease things
along.

In order to use this module, first I suggest you read this article:
http://www.angrydonuts.com/publishing_articles_a_tutorial

It contains an older version of this module; don't use that, instead
use this. This module, however, will do a lot of the work for you.

Specifically, it'll help you create the roles and the workflow and
apply them a little better and set the default translations. However,
to use this module effectively, you will still need to:

1) create Views of content for your site that respect the Published workflow

and/or

2) set up access; this module does not hide unpublished content. You can
   use the na_arbitrator module (in 4.7) and the workflow_access module
   that comes with it to hide content that is not in a published state.
   
3) Please use *Panels* instead of Dashboard.

----------------------------- Installing ------------------------------------

To install this module, drop it into the usual location. Be sure to also
have Views, Panels and Workflow. Activate this module at admin/modules
and then visit admin/settings/publishing

It is recommended that if this is all new to you, you play around on a test
site until you get the hang of what's going on first.